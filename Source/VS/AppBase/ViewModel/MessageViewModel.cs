﻿using AppBase.Common;
using AppBase.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBase.ViewModel
{
    public class MessageViewModel : ViewModelBase
    {
        public DelegateCommand ExitWindowCommand { get { return this.exitWindowCommand ?? (this.exitWindowCommand = new DelegateCommand(ExitWindow)); } }
        private DelegateCommand exitWindowCommand;
        private void ExitWindow()
        {
            ExitWindowMessenger.Notify();
        }

        public void SetMessage(MessageArgs messageArgs)
        {
            if (messageArgs == null) { throw new ArgumentNullException(nameof(messageArgs)); }

            switch (messageArgs.MessageType)
            {
            case MessageType.Ok: PositiveContent = "OK"; NegativeContent = ""; NegativeEnable = System.Windows.Visibility.Collapsed; break;
            case MessageType.OkCancel: PositiveContent = "OK"; NegativeContent = "Cancel"; break;
            case MessageType.YesNo: PositiveContent = "Yes"; NegativeContent = "No"; break;
            }
            Icon = messageArgs.IconType;
            switch(Icon)
            {
                case IconType.Information: System.Media.SystemSounds.Asterisk.Play(); break;
                case IconType.Warning: System.Media.SystemSounds.Beep.Play(); break;
                case IconType.Error: System.Media.SystemSounds.Hand.Play(); break;
            }
            Message = messageArgs.Message;
        }
        public IconType Icon { get { return this.icon; } set { this.icon = value; RaisePropertyChanged(nameof(Icon)); } }
        private IconType icon;
        public string Message { get { return this.message; } set { this.message = value; RaisePropertyChanged(nameof(Message)); } }
        private string message;

        public string PositiveContent { get { return this.positiveContent; } set { this.positiveContent = value; RaisePropertyChanged(nameof(PositiveContent)); } }
        private string positiveContent;
        public string NegativeContent { get { return this.negativeContent; } set { this.negativeContent = value; RaisePropertyChanged(nameof(NegativeContent)); } }
        private string negativeContent;
        public System.Windows.Visibility NegativeEnable { get; set; }

        public DelegateCommand SelectPotiveCommand { get { return this.selectPotiveCommand ?? (this.selectPotiveCommand = new DelegateCommand(SelectPotive)); } }
        private DelegateCommand selectPotiveCommand;
        private void SelectPotive()
        {
            SetWindwoResultAndExitMessenger.Notify(new Message<bool>(true), null);
        }
        public DelegateCommand SelectNegativeCommand { get { return this.selectNegativeCommand ?? (this.selectNegativeCommand = new DelegateCommand(SelectNegative)); } }
        private DelegateCommand selectNegativeCommand;
        private void SelectNegative()
        {
            SetWindwoResultAndExitMessenger.Notify(new Message<bool>(false), null);
        }
    }
}
