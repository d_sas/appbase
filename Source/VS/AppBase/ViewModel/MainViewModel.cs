﻿using AppBase.Common;
using AppBase.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBase.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public DelegateCommand ExitWindowCommand { get { return this.exitWindowCommand ?? (this.exitWindowCommand = new DelegateCommand(ExitWindow)); } }
        private DelegateCommand exitWindowCommand;
        private void ExitWindow()
        {
            AlertMessenger.Notify(
                new Message<MessageArgs>(new MessageArgs(MessageType.OkCancel, IconType.Warning, "", "アプリケーションを終了しますがよろしいですか？")),
                result =>
                {
                    if (result)
                    {
                        ExitWindowMessenger.Notify();
                    }
                });
        }

        public DelegateCommand NoneMessageCommand { get { return this.noneMessageCommand ?? (this.noneMessageCommand = new DelegateCommand(NoneMessage)); } }
        private DelegateCommand noneMessageCommand;
        private void NoneMessage()
        {
            AlertMessenger.Notify(
                new Message<MessageArgs>(new MessageArgs(MessageType.Ok, IconType.None, "", "None")),
                null);
        }

        public DelegateCommand InformationMessageCommand { get { return this.informationMessageCommand ?? (this.informationMessageCommand = new DelegateCommand(InformationMessage)); } }
        private DelegateCommand informationMessageCommand;
        private void InformationMessage()
        {
            AlertMessenger.Notify(
                new Message<MessageArgs>(new MessageArgs(MessageType.Ok, IconType.Information, "", "Information")),
                null);
        }

        public DelegateCommand WarningMessageCommand { get { return this.warningMessageCommand ?? (this.warningMessageCommand = new DelegateCommand(WarningMessage)); } }
        private DelegateCommand warningMessageCommand;
        private void WarningMessage()
        {
            AlertMessenger.Notify(
                new Message<MessageArgs>(new MessageArgs(MessageType.Ok, IconType.Warning, "", "Warning")),
                null);
        }

        public DelegateCommand ErrorMessageCommand { get { return this.errorMessageCommand ?? (this.errorMessageCommand = new DelegateCommand(ErrorMessage)); } }
        private DelegateCommand errorMessageCommand;
        private void ErrorMessage()
        {
            AlertMessenger.Notify(
                new Message<MessageArgs>(new MessageArgs(MessageType.Ok, IconType.Error, "", "Error")),
                null);
        }

        public DelegateCommand ShowSingleMessageCommand { get { return this.showSingleMessageCommand ?? (this.showSingleMessageCommand = new DelegateCommand(ShowSingleMessage)); } }
        private DelegateCommand showSingleMessageCommand;
        private void ShowSingleMessage()
        {
            AlertMessenger.Notify(
                new Message<MessageArgs>(new MessageArgs(MessageType.Ok, IconType.None, "", "こうなりました")),
                null);
        }
        public DelegateCommand ShowAlternativeMessageCommand { get { return this.showAlternativeMessageCommand ?? (this.showAlternativeMessageCommand = new DelegateCommand(ShowAlternativeMessage)); } }
        private DelegateCommand showAlternativeMessageCommand;
        private void ShowAlternativeMessage()
        {
            AlertMessenger.Notify(
                new Message<MessageArgs>(new MessageArgs(MessageType.OkCancel, IconType.None, "", "こうなりました")),
                null);
        }
    }
}
