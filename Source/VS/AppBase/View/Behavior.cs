﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Interop;

namespace AppBase.View
{
    public class DisableAltF4Behavior : Behavior<Window>
    {
        private int WM_SYSKEYDOWN = 0x0104;
        private int VK_F4 = 0x73;

        protected override void OnAttached()
        {
            this.AssociatedObject.SourceInitialized += (sender, e) =>
            {
                var handle = new WindowInteropHelper(this.AssociatedObject).Handle;
                var hwndSource = HwndSource.FromHwnd(handle);
                hwndSource.AddHook(this.WndProc);
            };
       }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_SYSKEYDOWN & wParam.ToInt32() == VK_F4)
            {
                handled = true;
            }

            return IntPtr.Zero;
        }
    }

    public class ResizeCaptionBehavior : Behavior<Grid>
    {
        public ResizeCaptionBehavior()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += MouseLeftButtonDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.MouseLeftButtonDown -= MouseLeftButtonDown;
        }

        private void MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
                {
                    switch (window.WindowState)
                    {
                    case WindowState.Normal: window.WindowState = WindowState.Maximized; break;
                    case WindowState.Maximized: window.WindowState = WindowState.Normal; break;
                    case WindowState.Minimized: break;
                    }
                }
                else
                {
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        window.DragMove();
                    }
                }
            }
        }
    }

    public class StableCaptionBehavior : Behavior<Grid>
    {
        public StableCaptionBehavior()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += MouseLeftButtonDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.MouseLeftButtonDown -= MouseLeftButtonDown;
        }

        private void MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                if (e.ButtonState == MouseButtonState.Pressed)
                {
                    window.DragMove();
                }
            }
        }
    }


    public class ResizeWindowButtonBehavior : Behavior<Button>
    {
        public ResizeWindowButtonBehavior()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Click += ResizeWindowButtonClick;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.Click -= ResizeWindowButtonClick;
        }

        private void ResizeWindowButtonClick(object sender, RoutedEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                switch (window.WindowState)
                {
                case WindowState.Normal: window.WindowState = WindowState.Maximized; break;
                case WindowState.Maximized: window.WindowState = WindowState.Normal; break;
                case WindowState.Minimized: break;
                }
            }
        }
    }

    public class MinimizedWindowButtonBehavior : Behavior<Button>
    {
        public MinimizedWindowButtonBehavior()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Click += MinimizedWindowButtonClick;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.Click -= MinimizedWindowButtonClick;
        }

        private void MinimizedWindowButtonClick(object sender, RoutedEventArgs e)
        {
            var obj = sender as DependencyObject;
            if (obj != null)
            {
                var window = Window.GetWindow(obj);
                switch (window.WindowState)
                {
                case WindowState.Normal: window.WindowState = WindowState.Minimized; break;
                case WindowState.Maximized: window.WindowState = WindowState.Minimized; break;
                case WindowState.Minimized: break;
                }
            }
        }
    }
}
