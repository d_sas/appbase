﻿using AppBase.Common;
using AppBase.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;

namespace AppBase.View
{
    public class ExitWindowAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var window = Window.GetWindow(AssociatedObject);
            window.Close();
        }
    }

    public class SetResultAndCloseAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var args = parameter as MessageActionEventArgs<bool>;
            if (args != null)
            {
                var result = args.Message.Body;
                var window = Window.GetWindow(AssociatedObject);
                window.DialogResult = result;
            }
        }
    }

    public class AlertAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            var args = parameter as MessageFuncEventArgs<MessageArgs, bool>;
            if (args != null)
            {
                var dialog = new MessageView(args.Message.Body);
                dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                dialog.Owner = Window.GetWindow(AssociatedObject);
                var result = (bool)dialog.ShowDialog();
                args.Callback?.Invoke(result);
            }
        }
    }

}
