﻿using AppBase.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBase.Utility
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public MessengerAction ExitWindowMessenger { get { return this.exitWindowMessenger ?? (this.exitWindowMessenger = new MessengerAction()); } }
        private MessengerAction exitWindowMessenger;

        public MessengerAction<bool> SetWindwoResultAndExitMessenger { get { return this.setWindwoResultAndExitMessenger ?? (this.setWindwoResultAndExitMessenger = new MessengerAction<bool>()); } }
        private MessengerAction<bool> setWindwoResultAndExitMessenger;

        public MessengerFunc<MessageArgs, bool> AlertMessenger { get { return this.alertMessenger ?? (this.alertMessenger = new MessengerFunc<MessageArgs, bool>()); } }
        private MessengerFunc<MessageArgs, bool> alertMessenger;
    }
}
