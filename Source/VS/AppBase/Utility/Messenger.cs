﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interactivity;

namespace AppBase.Utility
{
    public class Message<TParam>
    {
        public TParam Body { get; private set; }

        public Message(TParam body)
        {
            Body = body;
        }
    }

    public class MessageActionEventArgs<TParam> : EventArgs
    {
        public Message<TParam> Message { get; private set; }
        public Action Callback { get; private set; }

        public MessageActionEventArgs(Message<TParam> message, Action callback)
        {
            Message = message;
            Callback = callback;
        }
    }

    public class MessageFuncEventArgs<TParam, TResult> : EventArgs
    {
        public Message<TParam> Message { get; private set; }
        public Action<TResult> Callback { get; private set; }

        public MessageFuncEventArgs(Message<TParam> message, Action<TResult> callback)
        {
            Message = message;
            Callback = callback;
        }
    }

    public class MessengerAction
    {
        public event EventHandler Raised;

        public void Notify()
        {
            Raised?.Invoke(null, null);
        }
    }

    public class MessengerAction<TParam>
    {
        public event EventHandler<MessageActionEventArgs<TParam>> Raised;

        public void Notify(Message<TParam> message, Action callback)
        {
            Raised?.Invoke(this, new MessageActionEventArgs<TParam>(message, callback));
        }
    }

    public class MessengerFunc<TParam, TResult>
    {
        public event EventHandler<MessageFuncEventArgs<TParam, TResult>> Raised;

        public void Notify(Message<TParam> message, Action<TResult> callback)
        {
            Raised?.Invoke(this, new MessageFuncEventArgs<TParam, TResult>(message, callback));
        }
    }

    public class MessageTrigger : EventTrigger
    {
        protected override string GetEventName()
        {
            return "Raised";
        }
    }
}
