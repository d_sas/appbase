﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppBase.Common
{
    public enum MessageType
    {
        Ok,
        OkCancel,
        YesNo,
    }

    public enum IconType
    {
        None,
        Information,
        Warning,
        Error,
    }

    public class MessageArgs
    {
        public MessageArgs(MessageType type, IconType iconType, string title, string message)
        {
            MessageType = type;
            IconType = iconType;
            Title = title;
            Message = message;
        }

        public MessageArgs(string title, string message)
        {
            MessageType = MessageType.Ok;
            IconType = IconType.None;
            Title = title;
            Message = message;
        }

        public MessageType MessageType { get; private set; }
        public IconType IconType { get; private set; }
        public string Title { get; private set; }
        public string Message { get; private set; }
    }
}
